# Start2Star apache_solr_attachment_s3 install notes

## Configure required modules - Solr Search and S3FS modules

These instructions assume that a Solr environment has been configured for the site. If not head to `/admin/config/search/apachesolr/settings` and add or configure your solr server.

The module also requires that the Amazon S3FS module is installed and working. Them Amazon S3FS module can be configured here `/admin/config/media/s3fs`

## PHP Requirements

Add `allow_url_fopen = 1`  to your server's php.ini

## Install the module

1. Disable and uninstall `Search Files in Attachments`. (drupal.org/project/search_file_attachments) Be sure to check for uninstall even if module is not enabled.
2. Disable module `Apache Solr Search Attachments`. (drupal.org/project/apachesolr_attachments) Be sure to check for uninstall even if module is not enabled.
3. Enable module `Apache Solr Search Attachments with S3FS support`

## Configure Module and test Tika text extraction

The Apache Solr project indexes text, however it is not able to read text embeded in many formats. For this Solr relies on the Apache Tika project that can extract metadata and text from over a thousand different file types (such as PPT, XLS, and PDF).  Although Tika ships with most Solr installs a tika JAR file is included with the module for ease of use.

### Configure Module and set files to be indexed :

1. Navigate to /admin/config/search/apachesolr/attachments
2. Set *Extract using* to `Tika (local java application with S3FS support)`
3. Modify *Tika directory path* to reflect your server's web root
4. Check that *Tika jar file* is set to `tika-app-1.13.jar`
5. Alternately, *Tika directory path* and *Tika jar file* can point to your own locally installed Tika JAR
6. Save the configuration

Set files to be indexed:

1. Navigate to /admin/config/search/apachesolr
2. Select `Other` under *File* and Save

Note: Other file types ( Audio, Image, Video ) may be present in the list of available files.  These file types have have been excluded by default in /admin/config/search/apachesolr/attachments *Excluded file extensions*

### Test Tika extraction:

1. Navigate to /admin/config/search/apachesolr/attachments
2. Click `Test your tika extraction on local file`. The message `Text can be succesfully extracted` should be displayed.
3. Click `Test your tika extraction on Amazon S3FS file`. The message `Text can be succesfully extracted` should be displayed.
4. If Tika extraction fails see *Troubleshooting Tika* below.

## Test attachment indexing

*Warning* - Full site indexing is both computationally and network intensive and can take a great deal of time.  It is highly probable on sites with a lot of content that you will need to rely on CRON to reindex.  It is recommended that testing be done with a limited amount of attachment content.

### Test attachment indexing

1. Verify that a content node exists with a file attachment and record a word that exists *in the attachment* & that the attachment is stored in your S3FS bucket.  You may want to create a new node with unique content in the attachment not normally found on your site. The module includes `/test/kraken.pdf` for testing.
2. Navigate to `/admin/config/search/apachesolr/attachments`
3. Click `Delete the attachments from the index`
4. Click `Clear the attachment text extraction cache`
5. Navigate to `/admin/config/search/apachesolr`
6. Click `Delete the Search & Solr Index`  *See warning above.*  You can skip this step if you added new content in step 1
7. Click `Queue all content for reindexing`
8. Click `Index all queued content` & wait.

Note that both `Index all queued content` and cron both send content to Solr, which queues incomming jobs and runs independently. It may take Solr some time to process all submitted jobs.

### Verify search results

Navigate to an existing search block and search for your term, or to `/search/site/my-search-term-that-is-in-attachment`.  Your attachment should be returned in search results.

### Troubleshooting Tika

If the Tika extraction test fails run tika from the command line to debug java installation, permission, or path issues.

Replace `/var/www/docroot` with path to your drupal install:
````
java '-Dfile.encoding=UTF8' -cp '/var/www/docroot/sites/all/modules/custom/apachesolr_attachments_s3' -jar '/var/www/docroot/sites/all/modules/custom/apachesolr_attachments_s3/tika-app-1.13.jar' -t '/var/www/docroot/sites/all/modules/custom/apachesolr_attachments_s3/tests/test-tika.pdf'
````
This should return:
````
Testing Apache Solr Attachments text extraction
````
